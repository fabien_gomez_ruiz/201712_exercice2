import numpy as np
import pandas as pd
from pprint import pprint

class Exercice:
    vessels = {
        'p': 5,
        'c': 4,
        'o': 3,
        's': 3,
        't': 2
    }

    NULL_CHARACTER = '~'

    # Lit une grille à partir d'un fichier spécifié en paramètre.
    def read_file(self, filename):
        df = pd.read_csv(filename, header=None)
        return df.values

    # Affiche la grille spécifiée en paramètre
    def visualize(self, grid):
        pprint(grid)

    def check_vessel(self, letter, streak, vessels):
        if(letter in vessels):
            return vessels[letter] == streak
        else:
           return False

    def get_vessels_from_rows(self, grid, vessels):
        valid_vessels = set()
        streak_letter = ''
        streak = 0

        for row in grid:
            #print 'row: %s' % row
            for letter in row:
                if(letter == streak_letter):
                    streak+=1
                else:
                    #print 'streak: %s %s' % (streak_letter, streak)
                    if(self.check_vessel(streak_letter, streak, vessels)):
                        valid_vessels.add(streak_letter)
                    
                    # Reset parameters
                    streak = 1       
                    streak_letter = letter

        return valid_vessels
                    
    def get_vessels_from_columns(self, grid, vessels):
        valid_vessels = set()
        grid_np = np.array(grid)

        streak_letter = ''
        streak = 0

        for column in range(grid_np.shape[1]):
            #print 'column: %s' % grid_np[:, column]
            for letter in grid_np[:, column]:
                if(letter == streak_letter):
                    streak+=1
                else:
                    #print 'streak: %s %s' % (streak_letter, streak)
                    if(self.check_vessel(streak_letter, streak, vessels)):
                        valid_vessels.add(streak_letter)
                    
                    # Reset parameters
                    streak = 1       
                    streak_letter = letter

        return valid_vessels

    def check_water(self, grid):
        (grid == self.NULL_CHARACTER).sum() == 83

    def check_vessels(self, grid, vessels):
        valid_vessels_rows = self.get_vessels_from_rows(grid, self.vessels)
        #print('valid_vessels_rows: %s' % valid_vessels_rows)

        valid_vessels_columns = self.get_vessels_from_columns(grid, self.vessels)
        #print('valid_vessels_columns: %s' % valid_vessels_columns)

        valid_vessels = valid_vessels_rows.union(valid_vessels_columns)

        #print('union : %s' % valid_vessels_rows.union(valid_vessels_columns))
        #print('vessels: %s' % set(vessels.keys()))

        return (len(valid_vessels_rows) + len(valid_vessels_columns) == len(vessels)) & (valid_vessels == set(vessels.keys()))


    def check_grid(self, grid):
        check_w = self.check_water(grid)

        if(check_w == False):
            return check_w

        check_v = self.check_vessels(grid, self.vessels)

        return check_v


