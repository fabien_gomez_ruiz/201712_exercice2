## Bataille Navale - Vérifier une grille
La bataille navale est un jeu dans lequel vous devez placer vos différents navires sur une grille 10x10. Ces navires doivent être placés soit **verticalement**, soit **horizontalement**.
Chaque navire est identifiable par une lettre et une taille.

* Tous les bateaux sont-ils présents 1 fois uniquement?
* Ont-ils la bonne taille ?

Dans cette exercice, vous devrez vérifier que la grille que nous vous fournirons est bien valide.
La grille est une matrice à 2 dimensions contenant des lettres.
Cette matrice est toujours de taille 10x10.

La mer est représentée par le caractère '~' alors que les bateaux ont tous une lettre associée.

|   | cases | lettre |
|---|---|---|
| porte-avions  |  5  | p  |
|  croiseur |  4 | c  | 
|  contre-torpilleur | 3 | o  |
|  sous-marin | 3 | s  |
|  torpilleur | 2  | t  |
|  case vide | 1  | ~  |


Votre fonction check_grid dans le fichier exercice_2 devra, à partir d'un tableau fourni en paramètre, retourner True ou False en fonction de la validité de la grille.

Nous vous fournirons une dizaine de grille de tests. Ces grilles ne seront pas celles utilisées pour l'évaluation.

## Aide
Nous vous fournissons également des fonctions pour vous aider dans votre tâche. Leur tâche est décrite en commentaire.

## Exemples
Voici quelques exemples de grilles et leur validité. Vous trouverez ces exemples dans le dossier inputs/ de l'exercice.

### Grilles valides
#### inputs/2.csv

|   |   |   |   |   |   |   |   |   |   |
|---|---|---|---|---|---|---|---|---|---|
| ~ | ~ | ~ | ~ | ~ | ~ | p | ~ | s | ~ |
| ~ | ~ | ~ | ~ | ~ | ~ | p | ~ | s | ~ |
| ~ | ~ | ~ | ~ | ~ | ~ | p | ~ | s | ~ |
| ~ | ~ | ~ | ~ | ~ | ~ | p | ~ | ~ | ~ |
| ~ | ~ | ~ | ~ | t | t | p | ~ | ~ | ~ |
| o | o | o | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| c | c | c | c | ~ | ~ | ~ | ~ | ~ | ~ |

### Grilles non valides

#### inputs/1.csv

|   |   |   |   |   |   |   |   |   |   |
|---|---|---|---|---|---|---|---|---|---|
| ~ | ~ | s | ~ | t | ~ | ~ | ~ | ~ | ~ |
| ~ | ~ | s | ~ | t | ~ | ~ | p | ~ | ~ |
| ~ | ~ | s | ~ | ~ | ~ | ~ | p | ~ | ~ |
| o | o | o | ~ | ~ | ~ | ~ | p | ~ | ~ |
| ~ | ~ | ~ | ~ | ~ | ~ | ~ | p | ~ | ~ |
| ~ | ~ | ~ | ~ | ~ | ~ | ~ | p | ~ | ~ |
| ~ | ~ | ~ | ~ | ~ | ~ | ~ | p | ~ | ~ |
| ~ | ~ | ~ | ~ | c | c | c | c | ~ | ~ |
| ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |

#### inputs/3.csv

|   |   |   |   |   |   |   |   |   |   |
|---|---|---|---|---|---|---|---|---|---|
| ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| s | s | s | s | ~ | ~ | ~ | ~ | ~ | ~ |
| ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| ~ | ~ | ~ | ~ | ~ | o | ~ | c | c | c |
| ~ | t | t | ~ | ~ | o | ~ | ~ | ~ | ~ |
| ~ | p | ~ | ~ | ~ | o | ~ | ~ | ~ | ~ |
| ~ | p | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| ~ | p | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| ~ | p | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| ~ | p | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |


#### inputs/4.csv

|   |   |   |   |   |   |   |   |   |   |
|---|---|---|---|---|---|---|---|---|---|
| ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| ~ | ~ | ~ | ~ | ~ | ~ | ~ | s | ~ | ~ |
| ~ | ~ | ~ | ~ | c | o | c | s | c | c |
| ~ | t | t | ~ | ~ | o | ~ | s | ~ | ~ |
| ~ | p | ~ | ~ | ~ | o | ~ | ~ | ~ | ~ |
| ~ | p | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| ~ | p | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| ~ | p | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |
| ~ | p | ~ | ~ | ~ | ~ | ~ | ~ | ~ | ~ |




