import numpy as np
import pandas as pd
from pprint import pprint

class Exercice2:
    grids = [
        {'filepath': 'sources/inputs/1.csv', 'is_valid': False},
        {'filepath': 'sources/inputs/2.csv', 'is_valid': True},
        {'filepath': 'sources/inputs/3.csv', 'is_valid': False},
        {'filepath': 'sources/inputs/4.csv', 'is_valid': False},
    ]

    vessels = {
        'p': 5,
        'c': 4,
        'o': 3,
        's': 3,
        't': 2
    }

    NULL_CHARACTER = '~' # Represent the sea

    # Lit une grille à partir d'un fichier spécifié en paramètre. Cette méthode est là pour vous aider. Elle n'a pas besoin d'être modifiée
    def read_file(self, filename):
        df = pd.read_csv(filename, header=None)
        return df.values

    # Affiche la grille spécifiée en paramètre. Cette méthode est là pour vous aider. Elle n'a pas besoin d'être modifiée
    def visualize(self, grid):
        pprint(grid)

    # Méthode à mettre à jour
    def check_grid(self, grid):
        return False
