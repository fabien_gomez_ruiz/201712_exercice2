#!/usr/bin/python3.5

from os import listdir
from os.path import isfile, join
import pandas as pd
import random
import unittest
from sources.exercice_2 import Exercice2 as Exercice2
from exercice import Exercice as Exercice

class Test(unittest.TestCase):

    grids = [
        {
            'name': 'open',
            'filepath': 'grids/open/'
        },
        {
            'name': 'generated',
            'filepath': 'grids/generated/'
        }
    ]

    def wrong_result(self, exercice, grid, test_result, result):
        print('Cette grille est %s mais ton algorithme nous retourne %s' % (result, test_result))
        exercice.visualize(grid) 


    def test_grids(self):
        try:
            exercice = Exercice()
            exercice2 = Exercice2()

            for grid in self.grids:

                # List files
                grids_path = [f for f in listdir(grid['filepath']) if isfile(join(grid['filepath'], f))]

                for grid_path in grids_path:
                    #print(grid_path)
                    test_grid = exercice.read_file(join(grid['filepath'], grid_path))

                    test_result = exercice2.check_grid(test_grid)
                    result = exercice.check_grid(test_grid)
                    
                    if(test_result != result):
                        self.wrong_result(exercice, test_grid, test_result, result)

                    self.assertEqual(test_result, result)

            
        except Exception:
            self.fail("Une erreur est remontée.")


if __name__ == '__main__':
    unittest.main()
